import Vue from 'vue';
import Lightbox from 'vue-pure-lightbox';
import vueScrollto from 'vue-scrollto';
import VueResource from 'vue-resource';
import axios from 'axios';
import App from './App.vue';
import router from './router';

Vue.config.productionTip = false;
Vue.use(vueScrollto);
Vue.use(Lightbox);
Vue.use(VueResource);
Vue.use(axios);

new Vue({
  router,
  render(h) { return h(App); },
}).$mount('#app');

import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Estate from './views/Estate.vue';
import Kontakt from './views/Kontakt.vue';

Vue.use(Router);

export default new Router({
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/osiedle',
      name: 'osiedle',
      component: Estate,
    },
    {
      path: '/kontakt',
      name: 'kontakt',
      component: Kontakt,
    },
  ],
  mode: 'history',
});
